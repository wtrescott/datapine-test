<div class="chart-options">
    <div class="chart-option chart-type chart-type-line" data-type="line">Line</div>
    <div class="chart-option chart-type chart-type-column" data-type="column">Column</div>
    <div class="chart-option chart-type chart-type-pie" data-type="pie">Pie</div>
    <div class="chart-option chart-type chart-type-expand expand-chart" data-type="expand" onclick="buttonClicked(this)">Expand Chart</div>
</div>
<div class="chart"></div>

<div id="overlay" class="chart-container">
	<div class="chart"></div>
</div>