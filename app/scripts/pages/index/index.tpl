<br>
<div class="welcome-div">
	<br>
	<h1 class="welcome-message">Welcome to the datapine junior test</h1>
	<br>
</div>
<div class="other-div"><h3>My Task</h3></div>
<div class="message-div">
	<ol>
		<li>I changed the font of the two pages to Arial to look more contemporary.</li>
		<li>I also adjusted the position of the menu choices within the header for better alignment.</li>
		<li>I also tried to make the page headings and contents look more aesthetic with styling changes.</li>
	</ol>
	<p>I'm not sure if I scored any extra points, but regarding the two problems below:</p>
	<ul class="ul-special">
		<li>Clicking on the <span class="important-text">Expand Chart</span> button causes the associated graph to expand into a lightbox view.</li>
		<li>If you look at <span class="important-text">Chart 4</span> you will notice that when it is switched to a pie chart, the data labels for the Pie Chart slices now display the correct labels (Point 1, Point 2, etc.).</li>
	</ul>
	<br>
</div>
<br><br><br>
<div class="chart-list"></div>