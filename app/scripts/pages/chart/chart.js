/*global require*/
'use strict';

define([
    'backbone',
    'layoutmanager',
    'widgets/chart/chart',
    'text!./chart.tpl'
],
function (Backbone, LayoutManager, ChartView, chartTemplate) {
    return Backbone.Layout.extend({
        template: chartTemplate,
        initialize: function (opts) {
            this.chartId = opts.chartId;
            this.canExpand = opts.canExpand;
            this.chartView = new ChartView({ chartId: this.chartId });
        },
        beforeRender: function () {
            this.setView('.chart-container', this.chartView);
        },
        afterRender: function () {
        }
    });
});