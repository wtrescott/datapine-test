/*global require*/
'use strict';

define([
    'backbone',
    'layoutmanager',
    '../chart/chart',
    'text!./index.tpl'
],
function (Backbone, LayoutManager, ChartView, indexTemplate) {
    return Backbone.Layout.extend({
        template: indexTemplate,
        initialize: function () {
        },
        beforeRender: function () {
            /* This will load all charts from a json source app/data/chart{i}.json */
            for (var i=0; i<4; i++) {
                this.insertView('.chart-list', new ChartView({ chartId: (i + 1), canExpand: true }));
            }
        },
        afterRender: function () {
        }
    });
});

function buttonClicked(el) {
	var image_href = "/app/images/logo.png";
	
	if ($('#lightbox').length > 0) { // #lightbox exists
		
		var chartcontent = $(el).parent().parent().html();
		
		//place href as img src value
		$('#content').html(chartcontent);
		
		//show lightbox window - you could use .show('fast') for a transition
		$('#lightbox').show();
	}
	else {
		var lightbox = 
		'<div id="lightbox" onclick="closeClicked()">' +
			'<p>Click to close</p>' +
			'<div id="content">' + //insert clicked link's href into img src
				$(el).parent().parent().html() +
			'</div>' +	
		'</div>';
		$('body').append(lightbox);
	}
}

function closeClicked() {
	$('#lightbox').hide();
}

