/*global require*/
'use strict';

define([
    'backbone',
    'layoutmanager',
    'text!./help.tpl'
],
function (Backbone, LayoutManager, helpTemplate) {
    return Backbone.Layout.extend({
        template: helpTemplate,
        initialize: function () {
        },
        beforeRender: function () {
        },
        afterRender: function () {
        }
    });
});