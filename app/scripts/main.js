/*global require*/
'use strict';

require.config({
    shim: {
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: [
                'underscore',
                'jquery'
            ],
            exports: 'Backbone'
        },
        bootstrap: {
            deps: ['jquery'],
            exports: 'jquery'
        }
    },
    paths: {
        jquery: '../bower_components/jquery/jquery',
        backbone: '../bower_components/backbone/backbone',
        layoutmanager: '../bower_components/layoutmanager/backbone.layoutmanager',
        underscore: '../bower_components/underscore/underscore',
        highcharts: '../bower_components/highcharts-release/highcharts',
        text: '../bower_components/requirejs-text/text',
		highslide: '../bower_components/highslide/index'
    }
});


require([
    'backbone',
    'layoutmanager',
    'App',
    'routers/JuniorRouter',
	'highslide'
], function (Backbone, LayoutManager, App, JuniorRouter, Highslide) {
    App.initialize(JuniorRouter);
});
